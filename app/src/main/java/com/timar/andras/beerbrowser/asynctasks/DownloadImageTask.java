package com.timar.andras.beerbrowser.asynctasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.timar.andras.beerbrowser.util.AsyncResponse;

import java.io.InputStream;

/**
 * Created by bandi on 4/23/2017.
 */

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    AsyncResponse<Bitmap> mListener;

    public DownloadImageTask(AsyncResponse<Bitmap> listener) {
        this.mListener = listener;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }


    protected void onPostExecute(Bitmap result) {
        mListener.processOutput(result);
    }
}