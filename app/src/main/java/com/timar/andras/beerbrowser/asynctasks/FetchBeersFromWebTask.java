package com.timar.andras.beerbrowser.asynctasks;

import android.os.AsyncTask;
import android.util.Log;

import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.entities.BeerResponse;
import com.timar.andras.beerbrowser.util.AsyncResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by bandi on 4/23/2017.
 */

public class FetchBeersFromWebTask extends AsyncTask<Void, Void, List<Beer>> {

    private final String searchKey;

    public FetchBeersFromWebTask(AsyncResponse<List<Beer>> listener, String searchKey) {
        this.mListener = listener;
        this.searchKey = searchKey;
    }

    AsyncResponse<List<Beer>> mListener;

    @Override
    protected List<Beer> doInBackground(Void... params) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            ResponseEntity<BeerResponse> responseEntity = restTemplate.getForEntity("http://api.brewerydb.com/v2/search?type=beer&key=0b9c653a4973ef907aba4182e0aeccc1&q=" + searchKey, BeerResponse.class);
            if (responseEntity != null) {
                return responseEntity.getBody().getData() == null ? new ArrayList<Beer>() : responseEntity.getBody().getData();
            }
        } catch (Exception e) {
            Log.e("ResultFragment", e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    @Override
    protected void onPostExecute(List<Beer> beers) {
        mListener.processOutput(beers);
    }
}
