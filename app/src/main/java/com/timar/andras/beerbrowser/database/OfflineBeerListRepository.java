package com.timar.andras.beerbrowser.database;

import android.content.Context;

import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.entities.User;

/**
 * Created by bandi on 5/1/2017.
 */
public class OfflineBeerListRepository {

    public OfflineBeerListRepository(Context context) {
        this.context = context;
    }

    Context context;

    public void saveBeerOffline(Beer beer, User user) {
        DatabaseHelper db = new DatabaseHelper(context);
        Long beerId = beer.getId();
        Long userId = user.getId();
        if (beer.getId() == null) {
            beerId = db.createBeer(beer);
        }
        db.createUserBeerConnection(beerId, userId);
        db.closeDB();
    }

    public void removeOfflineBeer(Beer beer, User user) {
        DatabaseHelper db = new DatabaseHelper(context);
        if (beer.getId() != null) {
            db.deleteUserBeerConnection(user.getId(), beer.getId());
        }
        boolean beerCheck = db.checkIfNoMoreConnectionsForBeer(beer.getId());
        if (!beerCheck) {
            db.deleteBeer(beer.getId());
        }
        db.closeDB();

    }

    public boolean isExistingConnection(User user, Beer beer) {
        DatabaseHelper db = new DatabaseHelper(context);
        boolean existingConnection = db.isExistingConnection(beer, user);
        db.closeDB();
        return existingConnection;
    }
}
