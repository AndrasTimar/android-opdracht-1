package com.timar.andras.beerbrowser;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.entities.User;
import com.timar.andras.beerbrowser.service.AuthenticationService;
import com.timar.andras.beerbrowser.service.BeerService;
import com.timar.andras.beerbrowser.util.AsyncResponse;
import com.timar.andras.beerbrowser.util.drawer.BeerBrowserDrawerActivity;
import com.timar.andras.beerbrowser.util.drawer.DrawerItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BeerBrowserDrawerActivity implements ResultFragment.OnMainFragmentInteractionListener,
        SearchFragment.OnSearchFragmentInteractionListener, DetailsFragment.OnDetailsFragmentInteractionListener, AsyncResponse<List<Beer>> {

    public User user;
    private BeerService beerService;
    private AuthenticationService authenticationService;

    public MainActivity() {
        super(R.layout.activity_main, R.id.nav_list_main, R.id.drawer_layout_main);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        beerService = new BeerService(getApplicationContext());
        authenticationService = new AuthenticationService(getSharedPreferences("BEERBROWSER_PREF", MODE_PRIVATE), getApplicationContext());
        Intent i = getIntent();
        user = (User) i.getSerializableExtra("user");

        getSupportFragmentManager().beginTransaction()
                .add(R.id.container, new SearchFragment())
                .commit();
    }

    @Override
    protected List<DrawerItem> getDrawerItemList() {
        List<DrawerItem> drawerItems = new ArrayList<>();
        drawerItems.add(new DrawerItem("Search") {
            @Override
            public void itemClickCallBack() {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new SearchFragment()).addToBackStack("search_frag")
                        .commit();
            }
        });
        drawerItems.add(new DrawerItem("Offline List") {
            @Override
            public void itemClickCallBack() {
                navigateToOfflineList();
            }
        });
        drawerItems.add(new DrawerItem("Log out") {
            @Override
            public void itemClickCallBack() {
                logout();
            }
        });

        return drawerItems;
    }


    @Override
    public void receiveSearchKey(String searchKey) {
        beerService.getBeersFromWebAsync(this, searchKey);
    }

    @Override
    public void navigateToShowDetails(Beer beer) {
        DetailsFragment detailsFragment = new DetailsFragment();
        detailsFragment.setUser(user);
        detailsFragment.setBeer(beer);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, detailsFragment).addToBackStack("showBeer")
                .commit();
    }

    @Override
    public void saveBeerToOfflineCollection(final Beer beer) {
        beerService.saveBeerOfflineAsync(beer, user);
        Toast.makeText(this, "Beer saved to offline collection", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void removeBeerFromOfflineCollection(final Beer beer) {
        beerService.removeOfflineBeerAsync(beer, user);
        Toast.makeText(this, "Beer removed from offline collection", Toast.LENGTH_SHORT).show();
        navigateToOfflineList();
    }

    private void navigateToOfflineList() {
        beerService.fetchOfflineBeersForUserAsync(this, user);
    }

    public void logout() {
        authenticationService.clearSharedPreferences();
        Intent i = new Intent(this, AuthenticationActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void processOutput(List<Beer> output) {
        ResultFragment resultFragment = new ResultFragment();
        resultFragment.setBeerList(output);
        getSupportFragmentManager().beginTransaction().addToBackStack("123")
                .replace(R.id.container, resultFragment)
                .commit();
    }
}
