package com.timar.andras.beerbrowser.service;

import android.content.Context;

import com.timar.andras.beerbrowser.asynctasks.FetchBeersFromWebTask;
import com.timar.andras.beerbrowser.asynctasks.FetchOfflineBeersTask;
import com.timar.andras.beerbrowser.database.OfflineBeerListRepository;
import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.entities.User;
import com.timar.andras.beerbrowser.util.AsyncResponse;

import java.util.List;

/**
 * Created by bandi on 5/15/2017.
 */

public class BeerService {

    //Should be injected
    OfflineBeerListRepository repository;

    Context context;

    public BeerService(Context context) {
        this.context = context;
        this.repository = new OfflineBeerListRepository(context);
    }

    public void fetchOfflineBeersForUserAsync(AsyncResponse<List<Beer>> mBeerListListener, User user) {
        new FetchOfflineBeersTask(user, mBeerListListener, context).execute();
    }

    public void saveBeerOfflineAsync(final Beer beer, final User user) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                repository.saveBeerOffline(beer, user);
            }
        }).start();
    }

    public void removeOfflineBeerAsync(final Beer beer, final User user) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                repository.removeOfflineBeer(beer, user);
            }
        }).start();
    }

    public void getBeersFromWebAsync(AsyncResponse<List<Beer>> asyncResponse, String searchKey) {
        new FetchBeersFromWebTask(asyncResponse, searchKey).execute();
    }

    public boolean isExistingConnection(User user, Beer beer) {
        return repository.isExistingConnection(user, beer);
    }
}
