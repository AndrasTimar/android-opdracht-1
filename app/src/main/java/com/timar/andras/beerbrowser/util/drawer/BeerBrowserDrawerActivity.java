package com.timar.andras.beerbrowser.util.drawer;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.timar.andras.beerbrowser.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bandi on 5/3/2017.
 */

public abstract class BeerBrowserDrawerActivity extends AppCompatActivity {
    final int activity_authentication;
    final int nav_list;
    final int drawer_layout;

    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;

    private List<DrawerItem> mDrawerItems;

    public BeerBrowserDrawerActivity(int activity_layout, int nav_list_id, int drawer_layout) {
        this.activity_authentication = activity_layout;
        this.nav_list = nav_list_id;
        this.drawer_layout = drawer_layout;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_authentication);

        this.mDrawerItems = getDrawerItemList();

        mDrawerList = (ListView) findViewById(nav_list);

        addDrawerItems();

        mDrawerLayout = (DrawerLayout) findViewById(drawer_layout);
        createOnItemClickListener();

        ActionBar supportActionBar = getSupportActionBar();
        assert supportActionBar != null;
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        supportActionBar.setHomeButtonEnabled(true);

        setUpDrawer();
    }

    protected abstract List<DrawerItem> getDrawerItemList();

    private void createOnItemClickListener() {
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDrawerItems.get(position).itemClickCallBack();
                mDrawerLayout.closeDrawer(Gravity.START);
            }
        });
    }

    protected void addDrawerItems() {

        List<String> itemStrings = new ArrayList<>();
        for (DrawerItem item : mDrawerItems) {
            itemStrings.add(item.getText());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, itemStrings);
        mDrawerList.setAdapter(adapter);
    }

    private void setUpDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open_drawer, R.string.close_drawer);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}
