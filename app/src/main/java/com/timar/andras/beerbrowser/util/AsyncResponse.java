package com.timar.andras.beerbrowser.util;

/**
 * Created by bandi on 5/2/2017.
 */

public interface AsyncResponse<T> {
    void processOutput(T output);
}
