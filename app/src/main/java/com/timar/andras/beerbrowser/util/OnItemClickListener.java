package com.timar.andras.beerbrowser.util;

/**
 * Created by bandi on 5/14/2017.
 */

public interface OnItemClickListener<T> {
    void onItemClick(T item);
}
