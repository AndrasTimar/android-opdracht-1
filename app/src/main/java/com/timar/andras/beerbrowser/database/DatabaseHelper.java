package com.timar.andras.beerbrowser.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.entities.Labels;
import com.timar.andras.beerbrowser.entities.User;

import java.util.ArrayList;
import java.util.List;

import static com.timar.andras.beerbrowser.database.DBContract.BEER_KEY_ABV;
import static com.timar.andras.beerbrowser.database.DBContract.BEER_KEY_DESCRIPTION;
import static com.timar.andras.beerbrowser.database.DBContract.BEER_KEY_LABEL_ICON;
import static com.timar.andras.beerbrowser.database.DBContract.BEER_KEY_LABEL_MEDIUM;
import static com.timar.andras.beerbrowser.database.DBContract.BEER_KEY_NAME;
import static com.timar.andras.beerbrowser.database.DBContract.BEER_KEY_SERVING_TEMP;
import static com.timar.andras.beerbrowser.database.DBContract.CONNECTOR_KEY_BEER_ID;
import static com.timar.andras.beerbrowser.database.DBContract.CONNECTOR_KEY_USER_ID;
import static com.timar.andras.beerbrowser.database.DBContract.KEY_ID;
import static com.timar.andras.beerbrowser.database.DBContract.TABLE_BEER;
import static com.timar.andras.beerbrowser.database.DBContract.TABLE_USER;
import static com.timar.andras.beerbrowser.database.DBContract.TABLE_USER_BEER_CONNECTOR;
import static com.timar.andras.beerbrowser.database.DBContract.USER_KEY_PASSWORD;
import static com.timar.andras.beerbrowser.database.DBContract.USER_KEY_USERNAME;

/**
 * Created by bandi on 5/15/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, DBContract.DATABASE_NAME, null, DBContract.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DBContract.CREATE_TABLE_USER);
        db.execSQL(DBContract.CREATE_TABLE_BEER);
        db.execSQL(DBContract.CREATE_TABLE_USER_BEER_CONNECTOR);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DBContract.TABLE_USER_BEER_CONNECTOR);
        db.execSQL("DROP TABLE IF EXISTS " + DBContract.TABLE_BEER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);

        onCreate(db);
    }

    public long createBeer(Beer beer) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBContract.BEER_KEY_NAME, beer.getName());
        values.put(DBContract.BEER_KEY_ABV, beer.getAbv());
        values.put(DBContract.BEER_KEY_DESCRIPTION, beer.getDescription());
        values.put(DBContract.BEER_KEY_SERVING_TEMP, beer.getServingTemperatureDisplay());
        Labels labels = beer.getLabels();
        if (labels != null) {
            values.put(DBContract.BEER_KEY_LABEL_ICON, labels.getIcon());
            values.put(DBContract.BEER_KEY_LABEL_MEDIUM, labels.getMedium());
        }
        long id = db.insert(DBContract.TABLE_BEER, null, values);

        return id;
    }

    public Beer getBeerById(long id) {
        SQLiteDatabase db = getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DBContract.TABLE_BEER + " WHERE "
                + KEY_ID + " = " + id;
        Cursor c = db.rawQuery(selectQuery, null);
        return getBeerFromCursor(c);
    }

    public void deleteBeer(long beerId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BEER, KEY_ID + " = ?",
                new String[]{String.valueOf(beerId)});
    }

    public long createUser(User user) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBContract.USER_KEY_USERNAME, user.getUsername());
        values.put(DBContract.USER_KEY_PASSWORD, user.getPassword());

        long id = db.insert(TABLE_USER, null, values);

        return id;
    }

    public User getUserByNameAndPassword(String userName, String password) {

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DBContract.TABLE_USER + " WHERE "
                + USER_KEY_PASSWORD + " = ? and " + USER_KEY_USERNAME + " = ?";

        Cursor c = db.rawQuery(selectQuery, new String[]{userName, password});

        User user = null;
        if (c != null) {
            if (c.moveToFirst()) {
                user = new User();
                user.setUsername(userName);
                user.setPassword(password);
                user.setId(Long.valueOf(c.getInt(c.getColumnIndex(KEY_ID))));
            }
        }
        return user;
    }

    public void deleteUser(long userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USER, KEY_ID + " = ?",
                new String[]{String.valueOf(userId)});
    }

    public long createUserBeerConnection(long userId, long beerId) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBContract.CONNECTOR_KEY_BEER_ID, userId);
        values.put(DBContract.CONNECTOR_KEY_USER_ID, beerId);

        long id = db.insert(TABLE_USER_BEER_CONNECTOR, null, values);

        return id;
    }

    public List<Beer> getUserBeersByUserId(Long userId) {

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DBContract.TABLE_USER_BEER_CONNECTOR + " WHERE "
                + CONNECTOR_KEY_USER_ID + " = " + userId.toString();

        Cursor c = db.rawQuery(selectQuery, null);
        List<Beer> beersOfUser = new ArrayList<>();
        if (c != null)
            if (c.moveToFirst()) {
                do {
                    beersOfUser.add(getBeerById(c.getLong(c.getColumnIndex(CONNECTOR_KEY_BEER_ID))));
                } while ((c.moveToNext()));
            }
        return beersOfUser;
    }

    public void deleteUserBeerConnection(long userId, long beerId) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_USER_BEER_CONNECTOR, CONNECTOR_KEY_USER_ID + " = ? and " + CONNECTOR_KEY_BEER_ID + " = ?",
                new String[]{String.valueOf(userId), String.valueOf(beerId)});
    }

    public boolean checkIfNoMoreConnectionsForBeer(long beerId) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DBContract.TABLE_USER_BEER_CONNECTOR + " WHERE "
                + CONNECTOR_KEY_BEER_ID + " = " + beerId;

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            if (c.moveToFirst()) {
                return true;
            }
        }
        return false;

    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public boolean isExistingConnection(Beer inputBeer, User user) {
        SQLiteDatabase db = this.getReadableDatabase();
        Beer beer = getBeerByName(inputBeer.getName());
        if (beer != null) {
            String selectQuery = "SELECT  * FROM " + DBContract.TABLE_USER_BEER_CONNECTOR + " WHERE "
                    + CONNECTOR_KEY_BEER_ID + " = " + beer.getId() + " and " + CONNECTOR_KEY_USER_ID + " = " + user.getId();

            Cursor c = db.rawQuery(selectQuery, null);

            if (c != null) {
                if (c.moveToFirst()) {
                    return true;
                }
            }
        }
        return false;
    }

    private Beer getBeerByName(String name) {
        SQLiteDatabase db = getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + DBContract.TABLE_BEER + " WHERE "
                + BEER_KEY_NAME + " =  ?";
        Cursor c = db.rawQuery(selectQuery, new String[]{name});
        return getBeerFromCursor(c);
    }

    @Nullable
    private Beer getBeerFromCursor(Cursor c) {
        Beer beer = null;
        if (c != null)
            if (c.moveToFirst()) {
                beer = new Beer();
                beer.setId(Long.valueOf(c.getInt(c.getColumnIndex(KEY_ID))));
                beer.setAbv(c.getString(c.getColumnIndex(BEER_KEY_ABV)));
                beer.setDescription(c.getString(c.getColumnIndex(BEER_KEY_DESCRIPTION)));
                beer.setName(c.getString(c.getColumnIndex(BEER_KEY_NAME)));
                beer.setServingTemperatureDisplay(c.getString(c.getColumnIndex(BEER_KEY_SERVING_TEMP)));

                String labelIcon = c.getString(c.getColumnIndex(BEER_KEY_LABEL_ICON));
                String labelMedium = c.getString(c.getColumnIndex(BEER_KEY_LABEL_MEDIUM));
                if (labelIcon != null && labelMedium != null) {
                    Labels label = new Labels();
                    label.setIcon(labelIcon);
                    label.setMedium(labelMedium);
                    beer.setLabels(label);
                }
            }
        return beer;
    }
}
