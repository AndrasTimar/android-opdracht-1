package com.timar.andras.beerbrowser.service;

import android.graphics.Bitmap;

import com.timar.andras.beerbrowser.asynctasks.DownloadImageTask;
import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.util.AsyncResponse;

/**
 * Created by bandi on 5/15/2017.
 */

public class ImageService {

    public void downloadBeerImageFromWebAsync(AsyncResponse<Bitmap> listener, Beer beer) {
        new DownloadImageTask(listener).execute(beer.getLabels().getMedium());
    }

}
