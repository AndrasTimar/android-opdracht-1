package com.timar.andras.beerbrowser.database;

/**
 * Created by bandi on 5/15/2017.
 */

class DBContract {
    static final String TABLE_BEER = "beer";

    static final String TABLE_USER = "user";

    static final String TABLE_USER_BEER_CONNECTOR = "user_beer";

    static final String KEY_ID = "id";

    static final String USER_KEY_USERNAME = "username";

    static final String USER_KEY_PASSWORD = "password";

    static final String BEER_KEY_NAME = "name";

    static final String BEER_KEY_DESCRIPTION = "desc";

    static final String BEER_KEY_ABV = "abv";

    static final String BEER_KEY_LABEL_ID = "label_id";

    static final String BEER_KEY_SERVING_TEMP = "serv_temp";

    static final String BEER_KEY_LABEL_ICON = "icon";

    static final String BEER_KEY_LABEL_MEDIUM = "medium";

    static final String CONNECTOR_KEY_USER_ID = "user_id";

    static final String CONNECTOR_KEY_BEER_ID = "beer_id";

    //TODO try foreign keys

    static final String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_USER + "(" +
            KEY_ID + " integer primary key autoincrement," +
            USER_KEY_USERNAME + " text not null," +
            USER_KEY_PASSWORD + " text not null);";

    static final String CREATE_TABLE_BEER = "CREATE TABLE " + TABLE_BEER + "(" +
            KEY_ID + " integer primary key autoincrement," +
            BEER_KEY_NAME + " text not null," +
            BEER_KEY_DESCRIPTION + " text," +
            BEER_KEY_ABV + " text," +
            BEER_KEY_LABEL_ID + " integer," +
            BEER_KEY_SERVING_TEMP + " text," +
            BEER_KEY_LABEL_ICON + " text," +
            BEER_KEY_LABEL_MEDIUM + " text);";


    static final String CREATE_TABLE_USER_BEER_CONNECTOR = "CREATE TABLE " + TABLE_USER_BEER_CONNECTOR + "(" +
            KEY_ID + " integer primary key autoincrement," +
            CONNECTOR_KEY_USER_ID + " integer not null," +
            CONNECTOR_KEY_BEER_ID + " integer not null);";

    static final String DATABASE_NAME = "beerbrowser.db";
    static final int DATABASE_VERSION = 2;
}
