package com.timar.andras.beerbrowser;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timar.andras.beerbrowser.adapters.RecyclerBeerAdapter;
import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.util.OnItemClickListener;

import java.util.List;


/**
 * Created by bandi on 4/23/2017.
 */

public class ResultFragment extends Fragment {

    private OnMainFragmentInteractionListener mListener;
    private List<Beer> beerList;

    public ResultFragment() {
        //Required
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_result, container, false);
        if (beerList != null) {
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.listBeers);
            RecyclerBeerAdapter adapter = new RecyclerBeerAdapter(getActivity(), beerList, new OnItemClickListener<Beer>() {
                @Override
                public void onItemClick(Beer item) {
                    mListener.navigateToShowDetails(item);
                }
            });
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
        }
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMainFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginFragmentInteractionListener");
        }
    }

    public void setBeerList(@NonNull List<Beer> beerList) {
        this.beerList = beerList;
    }

    interface OnMainFragmentInteractionListener {
        void navigateToShowDetails(Beer beer);
    }
}
