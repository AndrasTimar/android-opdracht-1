package com.timar.andras.beerbrowser.util;

/**
 * Created by bandi on 5/15/2017.
 */

public class LoginCredentials {

    private String username;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
