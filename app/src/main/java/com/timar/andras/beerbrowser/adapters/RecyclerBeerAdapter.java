package com.timar.andras.beerbrowser.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.timar.andras.beerbrowser.R;
import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.service.ImageService;
import com.timar.andras.beerbrowser.util.AppStatus;
import com.timar.andras.beerbrowser.util.AsyncResponse;
import com.timar.andras.beerbrowser.util.OnItemClickListener;

import java.util.List;

/**
 * Created by bandi on 5/13/2017.
 */

public class RecyclerBeerAdapter extends RecyclerView.Adapter<RecyclerBeerAdapter.ViewHolder> {

    private List<Beer> beerList;

    private OnItemClickListener<Beer> onItemClickListener;
    private Context mContext;
    private ImageService imageService;

    public RecyclerBeerAdapter(Context context, List<Beer> beerList, OnItemClickListener<Beer> onItemClickListener) {
        this.mContext = context;
        this.beerList = beerList;
        this.onItemClickListener = onItemClickListener;
        this.imageService = new ImageService();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.listviewitem_beer, null);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int position) {

        final Beer beer = beerList.get(position);

        viewHolder.tvBeerDisplayName.setText(beer.getName());
        viewHolder.tvBeerAbv.setText((beer.getAbv() == null ? "?" : beer.getAbv()) + "% abv");

        if (beer.getLabels() != null && AppStatus.getInstance(mContext).isOnline()) {
            try {
                imageService.downloadBeerImageFromWebAsync(new AsyncResponse<Bitmap>() {
                    @Override
                    public void processOutput(Bitmap output) {
                        viewHolder.ivBeerIcon.setImageBitmap(output);
                    }
                }, beer);
            } catch (Exception ignored) {
            }
        }
        final View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(beer);
            }
        };
        viewHolder.itemView.setOnClickListener(listener);
    }

    @Override
    public int getItemCount() {
        return (null != beerList ? beerList.size() : 0);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivBeerIcon;
        TextView tvBeerDisplayName;
        TextView tvBeerAbv;

        ViewHolder(View itemView) {
            super(itemView);
            tvBeerDisplayName = (TextView) itemView.findViewById(R.id.tvNameListItemBeer);
            tvBeerAbv = (TextView) itemView.findViewById(R.id.tvAbvListItemBeer);
            ivBeerIcon = (ImageView) itemView.findViewById(R.id.imvListItemBeer);
        }
    }
}
