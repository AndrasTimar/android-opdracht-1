package com.timar.andras.beerbrowser.service;

import android.content.Context;
import android.content.SharedPreferences;

import com.timar.andras.beerbrowser.database.UserRepository;
import com.timar.andras.beerbrowser.entities.User;
import com.timar.andras.beerbrowser.util.LoginCredentials;

/**
 * Created by bandi on 5/15/2017.
 */

public class AuthenticationService {

    SharedPreferences preferences;
    UserRepository userRepository;

    public AuthenticationService(SharedPreferences preferences, Context context) {

        this.preferences = preferences;
        this.userRepository = new UserRepository(context);
    }

    public LoginCredentials getCurrentCredentialsFromPreferences() {
        String username = preferences.getString("username", "");
        String password = preferences.getString("password", "");
        LoginCredentials credentials = null;
        if (!username.equals("") && !password.equals("")) {
            credentials = new LoginCredentials();
            credentials.setUsername(username);
            credentials.setPassword(password);
        }
        return credentials;
    }

    public void saveCredentialsToPreferences(LoginCredentials credentials) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username", credentials.getUsername());
        editor.putString("password", credentials.getPassword());
        editor.commit();
    }

    public User tryLogin(LoginCredentials credentials) {
        User user = null;
        if (credentials != null) {
            user = findUserByCredentials(credentials);
            if (user != null) {
                saveCredentialsToPreferences(credentials);
            }
        }
        return user;
    }

    private User findUserByCredentials(LoginCredentials credentials) {
        User user = userRepository.findUserByUsernameAndPassworD(credentials.getUsername(), credentials.getPassword());
        return user;
    }

    public boolean registerNewUser(LoginCredentials credentials) {
        if (findUserByCredentials(credentials) == null) {
            User newUser = new User();
            newUser.setUsername(credentials.getUsername());
            newUser.setPassword(credentials.getPassword());
            userRepository.persistUser(newUser);
            return true;
        }
        return false;
    }

    public void clearSharedPreferences() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("username", "");
        editor.putString("password", "");
        editor.commit();
    }

}
