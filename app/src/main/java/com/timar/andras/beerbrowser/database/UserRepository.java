package com.timar.andras.beerbrowser.database;

import android.content.Context;

import com.timar.andras.beerbrowser.entities.User;

/**
 * Created by bandi on 5/15/2017.
 */

public class UserRepository {

    Context context;

    public UserRepository(Context context) {
        this.context = context;
    }

    public User findUserByUsernameAndPassworD(String username, String password) {
        DatabaseHelper db = new DatabaseHelper(context);
        User user = db.getUserByNameAndPassword(username, password);
        db.closeDB();
        return user;
    }

    public void persistUser(User user) {
        DatabaseHelper db = new DatabaseHelper(context);
        db.createUser(user);
        db.closeDB();
    }
}
