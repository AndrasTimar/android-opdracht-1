package com.timar.andras.beerbrowser.asynctasks;

import android.content.Context;
import android.os.AsyncTask;

import com.timar.andras.beerbrowser.database.DatabaseHelper;
import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.entities.User;
import com.timar.andras.beerbrowser.util.AsyncResponse;

import java.util.List;

/**
 * Created by bandi on 5/1/2017.
 */

public class FetchOfflineBeersTask extends AsyncTask<Void, Void, List<Beer>> {

    private User user;
    private AsyncResponse<List<Beer>> mListener;
    private Context context;

    public FetchOfflineBeersTask(User user, AsyncResponse<List<Beer>> mListener, Context context) {
        this.user = user;
        this.mListener = mListener;
        this.context = context;
    }

    @Override
    protected List<Beer> doInBackground(Void... params) {
        DatabaseHelper db = new DatabaseHelper(context);
        List<Beer> userBeersByUserId = db.getUserBeersByUserId(user.getId());
        db.closeDB();
        return userBeersByUserId;
    }

    @Override
    protected void onPostExecute(List<Beer> beers) {
        mListener.processOutput(beers);
    }
}
