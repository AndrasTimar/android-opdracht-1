package com.timar.andras.beerbrowser;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.timar.andras.beerbrowser.util.LoginCredentials;

public class RegisterFragment extends Fragment {

    private View rootView;
    private OnRegisterFragmentInteractionListener mListener;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_register, container, false);
        rootView.findViewById(R.id.btn_do_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRegisterButtonClick();
            }
        });
        return rootView;
    }

    private void onRegisterButtonClick() {
        String username = ((EditText) rootView.findViewById(R.id.et_reg_username)).getText().toString();
        String password = ((EditText) rootView.findViewById(R.id.et_reg_password)).getText().toString();
        String passwordConfirm = ((EditText) rootView.findViewById(R.id.et_reg_password_confirm)).getText().toString();
        if ("".equals(username) || "".equals(password) || "".equals(passwordConfirm)) {
            Toast.makeText(getActivity(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!password.equals(passwordConfirm)) {
            Toast.makeText(getActivity(), "Passwords did not match", Toast.LENGTH_SHORT).show();
            return;
        }
        LoginCredentials credentials = new LoginCredentials();
        credentials.setUsername(username);
        credentials.setPassword(password);
        boolean success = mListener.registerNewUser(credentials);

        if (!success) {
            Toast.makeText(getActivity(), "Username is already taken", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisterFragmentInteractionListener) {
            mListener = (OnRegisterFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRegisterFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    interface OnRegisterFragmentInteractionListener {
        boolean registerNewUser(LoginCredentials credentials);
    }
}
