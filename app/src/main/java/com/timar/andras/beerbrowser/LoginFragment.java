package com.timar.andras.beerbrowser;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.timar.andras.beerbrowser.util.LoginCredentials;


public class LoginFragment extends Fragment {

    private OnLoginFragmentInteractionListener mListener;
    View rootView;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_login, container, false);
        rootView.findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoginButtonClick();
            }
        });
        rootView.findViewById(R.id.btn_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onRegisterButtonClick();
            }
        });
        return rootView;
    }

    private void onLoginButtonClick() {
        String username = ((EditText) rootView.findViewById(R.id.et_username)).getText().toString();
        String password = ((EditText) rootView.findViewById(R.id.et_password)).getText().toString();
        LoginCredentials credentials = new LoginCredentials();
        credentials.setPassword(password);
        credentials.setUsername(username);
        if (!mListener.login(credentials)) {
            Toast.makeText(getActivity(), "Invalid credentials", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLoginFragmentInteractionListener) {
            mListener = (OnLoginFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLoginFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    interface OnLoginFragmentInteractionListener {

        boolean login(LoginCredentials credentials);

        void onRegisterButtonClick();

    }
}
