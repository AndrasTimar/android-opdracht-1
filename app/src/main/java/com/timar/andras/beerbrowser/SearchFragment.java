package com.timar.andras.beerbrowser;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class SearchFragment extends Fragment {

    private SearchFragment.OnSearchFragmentInteractionListener mListener;
    private View rootView;

    public SearchFragment() {
        //Required
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search, container, false);
        Button searchButton = (Button) rootView.findViewById(R.id.search_button);
        final EditText etSearchKey = (EditText) rootView.findViewById(R.id.et_search_key);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSearchKey.getText() != null && !"".equals(etSearchKey.getText().toString())) {
                    mListener.receiveSearchKey(etSearchKey.getText().toString());
                }
            }
        });
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnSearchFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnLoginFragmentInteractionListener");
        }
    }

    interface OnSearchFragmentInteractionListener {
        void receiveSearchKey(String searchkey);
    }
}
