package com.timar.andras.beerbrowser.util.drawer;

/**
 * Created by bandi on 5/7/2017.
 */

public abstract class DrawerItem {

    public DrawerItem(String text) {
        this.setText(text);
    }

    private String text;

    public abstract void itemClickCallBack();

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
