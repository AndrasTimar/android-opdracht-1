package com.timar.andras.beerbrowser;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.timar.andras.beerbrowser.entities.Beer;
import com.timar.andras.beerbrowser.entities.User;
import com.timar.andras.beerbrowser.service.BeerService;
import com.timar.andras.beerbrowser.service.ImageService;
import com.timar.andras.beerbrowser.util.AppStatus;
import com.timar.andras.beerbrowser.util.AsyncResponse;


public class DetailsFragment extends Fragment implements AsyncResponse<Bitmap> {

    private Beer beer;
    private OnDetailsFragmentInteractionListener mListener;
    private ImageView beerImage;
    private User user;
    private ImageService imageService;
    private BeerService beerService;

    public DetailsFragment() {
        imageService = new ImageService();
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_details, container, false);

        beerService = new BeerService(getContext().getApplicationContext());

        Button listButton;
        if (!beerService.isExistingConnection(user, beer)) {
            listButton = (Button) rootView.findViewById(R.id.btn_save_offline);
            listButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.saveBeerToOfflineCollection(beer);
                }
            });
        } else {
            listButton = (Button) rootView.findViewById(R.id.btn_remove_offline);
            listButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.removeBeerFromOfflineCollection(beer);
                }
            });
        }
        listButton.setVisibility(View.VISIBLE);

        beerImage = (ImageView) rootView.findViewById(R.id.iv_details_beer_logo);
        TextView beerName = (TextView) rootView.findViewById(R.id.tv_beer_name_content);
        TextView beerDesc = (TextView) rootView.findViewById(R.id.tv_beer_description_content);
        TextView beerAbv = (TextView) rootView.findViewById(R.id.tv_abv_content);
        TextView beerServTemp = (TextView) rootView.findViewById(R.id.tv_servtemp_content);

        beerName.setText(beer.getName());
        beerDesc.setText(beer.getDescription());
        beerAbv.setText((beer.getAbv() == null ? "?" : beer.getAbv()) + "%");
        beerServTemp.setText(beer.getServingTemperatureDisplay());

        if (beer.getLabels() != null && AppStatus.getInstance(getActivity()).isOnline()) {
            try {
                imageService.downloadBeerImageFromWebAsync(this, beer);
            } catch (Exception ignored) {

            }
        }
        return rootView;
    }

    public void setBeer(Beer beer) {
        this.beer = beer;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDetailsFragmentInteractionListener) {
            mListener = (OnDetailsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDetailsFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void processOutput(Bitmap output) {
        this.beerImage.setImageBitmap(output);
    }


    interface OnDetailsFragmentInteractionListener {

        void saveBeerToOfflineCollection(Beer beer);

        void removeBeerFromOfflineCollection(Beer beer);
    }

}
