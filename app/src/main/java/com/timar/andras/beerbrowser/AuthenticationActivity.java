package com.timar.andras.beerbrowser;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.timar.andras.beerbrowser.entities.User;
import com.timar.andras.beerbrowser.service.AuthenticationService;
import com.timar.andras.beerbrowser.util.LoginCredentials;
import com.timar.andras.beerbrowser.util.drawer.BeerBrowserDrawerActivity;
import com.timar.andras.beerbrowser.util.drawer.DrawerItem;

import java.util.ArrayList;
import java.util.List;

public class AuthenticationActivity extends BeerBrowserDrawerActivity implements LoginFragment.OnLoginFragmentInteractionListener, RegisterFragment.OnRegisterFragmentInteractionListener {

    public AuthenticationActivity() {
        super(R.layout.activity_authentication, R.id.nav_list, R.id.drawer_layout);
    }

    private AuthenticationService authenticationService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        authenticationService = new AuthenticationService(getSharedPreferences("BEERBROWSER_PREF", MODE_PRIVATE), getApplicationContext());

        LoginCredentials credentials = authenticationService.getCurrentCredentialsFromPreferences();

        boolean success = login(credentials);
        if (!success) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.authcontainer, new LoginFragment())
                    .commit();
        }
    }

    @Override
    public boolean login(LoginCredentials credentials) {
        User user = authenticationService.tryLogin(credentials);
        boolean success = user != null;

        if (success) {
            loginSuccess(user);
        }
        return success;
    }

    public void loginSuccess(User user) {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("user", user);
        startActivity(i);
    }


    @Override
    protected List<DrawerItem> getDrawerItemList() {
        List<DrawerItem> drawerItems = new ArrayList<>();
        drawerItems.add(new DrawerItem("Login") {
            @Override
            public void itemClickCallBack() {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.authcontainer, new LoginFragment())
                        .addToBackStack("log_frag")
                        .commit();
            }
        });
        drawerItems.add(new DrawerItem("Register") {
            @Override
            public void itemClickCallBack() {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.authcontainer, new RegisterFragment())
                        .addToBackStack("reg_frag")
                        .commit();
            }
        });
        return drawerItems;
    }

    @Override
    public void onRegisterButtonClick() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.authcontainer, new RegisterFragment()).addToBackStack("login")
                .commit();
    }

    public void onRegistrationSuccess() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.authcontainer, new LoginFragment()).addToBackStack("register")
                .commit();
        Toast.makeText(this, "Registration successful", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean registerNewUser(LoginCredentials credentials) {
        if (authenticationService.registerNewUser(credentials)) {
            onRegistrationSuccess();
            return true;
        }
        return false;
    }
}
